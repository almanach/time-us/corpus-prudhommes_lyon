# Corpus des Purd'hommes de Lyon

## Description du corpus
Il s'agit d'un corpus de compte-rendus de séances du conseil des prud'hommes lyonnais publiés dans la presse ouvrière entre 1831 et 1850. Les images ont été téléchargées depuis [Numelyo](https://numelyo.bm-lyon.fr/) puis transcrite à l'aide du service Abby FineReader intégré à Transkribus. Les transcriptions ainsi obtenues ont été corrigées manuellement, puis un recensement des dates d'audience a été réalisée avant de découper les transcriptions en autant d'extraits.
Les fichiers XML TEI exportés depuis Transkribus, avant découpe par audience peuvent être fournis [sur demande](mailto:alix.chague@inria.fr).

## Description du fichier
Le fichier contient une ligne par audience.

- colonne 1 : l'identifiant Numelyo
- colonne 2 : le titre du journal
- colonne 3 : le numéro de publication
- colonne 4 : l'année de numérotation (0 si la numérotation est continue)
- colonne 5 : la date de publication (si 00-mm-aaaa : c'est que la publication ne le précise pas)
- colonne 6 : les numéro de pages sur lesquels se trouvent l'article de compte-rendu
- colonne 7 : la date de l'audience, noter qu'il peut y avoir plusieurs compte-rendus pour une même date (idem : si 00-mm-aaaa : c'est que la publication ne le précise pas)
- colonne 8 : la transcription du compte-rendu de l'audience (balisage très très léger)
- colonne 9 : la liste des contributeur-rices et leurs rôles associés
- colonne 10 : l'URL vers la ressource sur Numelyo
- colonne 11 : la liste des URL vers les images sources